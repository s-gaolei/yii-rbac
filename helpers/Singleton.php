<?php

namespace Gaolei\YiiRBAC\helpers;

/**
 * 单例模式
 * Trait Singleton
 * @package common\helpers
 */
trait Singleton
{
    private static $instance = null;

    protected function __construct(){}

    final protected function __clone(){}

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }
}