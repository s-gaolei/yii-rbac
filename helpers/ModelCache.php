<?php

namespace Gaolei\YiiRBAC\helpers;

/**
 * Trait ModelCache
 * @package common\helpers
 * @property string $cacheRelyFileName
 */
trait ModelCache
{

    /**
     * 保存数据之后 更新缓存
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->updateCache();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * 删除数据之后更新缓存
     */
    public function afterDelete()
    {
        $this->updateCache();
        parent::afterDelete();
    }

    /**
     * 主动更新缓存
     * @author: gaolei 2021/5/25 6:17 下午
     */
    public function updateCache()
    {
        Cache::init()->setRelyFile($this->cacheRelyFileName);
    }

}