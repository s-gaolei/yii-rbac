<?php

use yii\helpers\Html;

/* @var $this  yii\web\View */
/* @var $model Gaolei\YiiRBAC\models\BizRule */

$this->title = '添加规则';
?>
<div class="page-container">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', ['model' => $model,]); ?>
</div>
