<?php

/* @var $this yii\web\View */
/* @var $model Gaolei\YiiRBAC\models\AuthItem */
/* @var $context Gaolei\YiiRBAC\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', 'Update ' . $labels['Item']) . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', $labels['Items']), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'Update');
?>
<div class="page-container">
    <?= $this->render('_form', ['model' => $model,]); ?>
</div>
