<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Gaolei\YiiRBAC\models\AuthItem */
/* @var $context Gaolei\YiiRBAC\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', 'Create ' . $labels['Item']);
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', $labels['Items']), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-container">
    <?= $this->render('_form', ['model' => $model,]); ?>
</div>
