<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Gaolei\YiiRBAC\models\Menu */

$this->title = '修改菜单' . ': ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'Update');
?>
<div class="page-container">
    <?= $this->render('_form', ['model' => $model,]) ?>
</div>
