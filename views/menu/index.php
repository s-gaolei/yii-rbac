<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel Gaolei\YiiRBAC\models\search\Menu */

$this->title = '菜单管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-container">

    <div class="page-search">
        <?php $form = ActiveForm::begin(['action' => ['index'], 'method' => 'get', 'id' => 'page-search-form']); ?>
        <?= Html::activeInput('text', $searchModel, 'id', ['class' => 'form-control w80 dis-in-b', 'placeholder' => 'ID']) ?>
        <?= Html::activeInput('text', $searchModel, 'name', ['class' => 'form-control w100 dis-in-b', 'placeholder' => '名称']) ?>
        <?= Html::activeInput('text', $searchModel, 'route', ['class' => 'form-control w150 dis-in-b', 'placeholder' => '路由']) ?>
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end(); ?>
        <?= Html::a('添加菜单', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'menuParent.name',
                'filter' => Html::activeTextInput($searchModel, 'parent_name', [
                    'class' => 'form-control', 'id' => null
                ]),
                'label' => Yii::t('rbac-admin', '父级'),
            ],
            'route',
            'order',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
