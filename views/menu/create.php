<?php

/* @var $this yii\web\View */
/* @var $model Gaolei\YiiRBAC\models\Menu */

$this->title = '添加菜单';
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-container">
    <?= $this->render('_form', ['model' => $model,]) ?>
</div>
