<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model \Gaolei\YiiRBAC\models\form\Signup
 */
$this->title = '添加账户';
$this->params['breadcrumbs'][] = ['label' => '账户管理', 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;
$levels = \Gaolei\YiiRBAC\models\Adminer::levels();
$admin = \Yii::$app->user->identity;
if ($admin->getId() > 1) {
    foreach ($levels as $k => $str) {
        if ($k >= $admin->level) {
            unset($levels[$k]);
        }
    }
}
?>
<div class="page-container">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($model, 'username')->label('登陆名')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'realname')->label('真实姓名') ?>
            <?= $form->field($model, 'phone')->label('联系电话') ?>
            <?= $form->field($model, 'level')->label('权限等级')->dropDownList($levels) ?>
            <?= $form->field($model, 'password')->label('密码')->passwordInput() ?>
            <div class="form-group">
                <?= Html::submitButton('添加', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>