<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use Gaolei\YiiRBAC\models\form\ChangePwd;

/** @var View $this */
/** @var ChangePwd $model */
$this->title = '更新密码';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-container">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($model, 'old_pass')->label('原密码')->passwordInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'new_pass')->label('新密码')->passwordInput() ?>
            <?= $form->field($model, 'con_pass')->label('确认密码')->passwordInput() ?>
            <div class="form-group">
                <?= \yii\helpers\Html::submitButton('保存', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>