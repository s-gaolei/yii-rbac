<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Gaolei\YiiRBAC\components\Helper;

/* @var $this yii\web\View */
/* @var $model Gaolei\YiiRBAC\models\Adminer */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => '账户管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$controllerId = $this->context->uniqueId . '/';
$levels = $model::levels();
$states = $model::states();
?>
<div class="page-container">
    <p>
        <?php
        if ($model->state == $model::STATE_DISABLE && Helper::checkRoute($controllerId . 'activate')) {
            echo Html::a(Yii::t('rbac-admin', 'Activate'), ['activate', 'id' => $model->id], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => Yii::t('rbac-admin', 'Are you sure you want to activate this user?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?php
        if (Helper::checkRoute($controllerId . 'delete')) {
            echo Html::a(Yii::t('rbac-admin', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'realname',
            'phone',
            [
                'label' => '权限等级',
                'attribute' => 'level',
                'value' => function ($model) use ($levels) {
                    return $levels[$model->level];
                },
                'filter' => $levels
            ],
            [
                'label' => '权限等级',
                'attribute' => 'level',
                'value' => function ($model) use ($states) {
                    return $states[$model->state];
                },
                'filter' => $states
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>
