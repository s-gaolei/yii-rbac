var preg = /^[0-9A-Za-z]{6,16}$/

function resetPwd(id) {
    console.log(id)
    var content = prompt('请输入新的密码(字母、数字 6~16)');
    // 取消输入
    if (content === null) return;
    var pwd = $.trim(content);
    if (!preg.test(pwd)) {
        return resetPwd(id)
    }
    location.href = 'reset-pwd?id=' + id + '&pwd=' + pwd;
}