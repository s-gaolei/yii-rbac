<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use Gaolei\YiiRBAC\models\Adminer;
use Gaolei\YiiRBAC\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel Gaolei\YiiRBAC\models\search\AdminerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '账号管理';
$this->params['breadcrumbs'][] = $this->title;

$levels = Adminer::levels();
$states = Adminer::states();
\yii\web\YiiAsset::register($this);
$this->registerJs($this->render('_script.js'), $this::POS_HEAD);
$admin = \Yii::$app->getUser()->identity;
?>
<div class="page-container">

    <div class="page-search">
        <?php $form = ActiveForm::begin(['action' => ['index'], 'method' => 'get', 'id' => 'page-search-form']); ?>
        <?= Html::activeInput('text', $searchModel, 'id', ['class' => 'form-control w80 dis-in-b', 'placeholder' => 'ID']) ?>
        <?= Html::activeInput('text', $searchModel, 'username', ['class' => 'form-control w100 dis-in-b', 'placeholder' => '登录名']) ?>
        <?= Html::activeInput('text', $searchModel, 'realname', ['class' => 'form-control w150 dis-in-b', 'placeholder' => '姓名']) ?>
        <?= Html::activeInput('text', $searchModel, 'phone', ['class' => 'form-control w150 dis-in-b', 'placeholder' => '手机']) ?>
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end(); ?>
        <?= Html::a('添加账号', ['signup'], ['class' => 'btn btn-success']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'username',
            'realname',
            'phone',
            [
                'label' => '权限等级',
                'attribute' => 'level',
                'value' => function ($model) use ($levels) {
                    return $levels[$model->level];
                },
                'filter' => $levels
            ],
            [
                'attribute' => 'state',
                'format' => 'raw',
                'value' => function ($model) use ($states) {
                    $class = 'text-success';
                    if ($model->state === Adminer::STATE_DISABLE) {
                        $class = 'text-warning';
                    }
                    return '<span class="' . $class . '">' . $states[$model->state] . '</span>';
                },
                'filter' => $states
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Helper::filterActionColumn(['allot', 'active', 'disable', 'reset-pwd', 'view', 'delete']),
                'buttons' => [
                    'allot' => function ($url, $model) use ($admin) {
                        if ($admin->getId() === 1 || $admin->level > $model->level) {
                            return Html::a('授权', ['/admin/allot?id=' . $model->id], [
                                'class' => 'btn btn-xs btn-info'
                            ]);
                        }
                    },
                    'reset-pwd' => function ($url, $model) use ($admin) {
                        if ($admin->level > $model->level) {
                            return Html::button('重置密码', [
                                'class' => 'btn btn-xs btn-primary',
                                'onClick' => "resetPwd('{$model->id}')"
                            ]);
                        }
                        return '';
                    },
                    'active' => function ($url, $model) use ($admin) {
                        if (
                            $model->state === Adminer::STATE_ACTIVE ||
                            $admin->level <= $model->level
                        ) {
                            return '';
                        }
                        $options = [
                            'title' => '激活',
                            'aria-label' => '激活',
                            'data-confirm' => '确认激活此账户？',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                            'class' => 'btn btn-xs btn-success'
                        ];
                        return Html::a('激活', $url, $options);
                    },
                    'disable' => function ($url, $model) use ($admin) {
                        if (
                            $model->state == Adminer::STATE_DISABLE ||
                            $admin->level <= $model->level
                        ) {
                            return '';
                        }
                        $options = [
                            'title' => '禁用',
                            'aria-label' => '禁用',
                            'data-confirm' => '确认禁用此账户？',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                            'class' => 'btn btn-xs btn-warning'
                        ];
                        return Html::a('禁用', $url, $options);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
