<?php

namespace Gaolei\YiiRBAC\components;

use Yii;
use yii\web\User;
use yii\di\Instance;
use yii\base\Module;
use yii\web\ForbiddenHttpException;
use Gaolei\YiiRBAC\models\user\Admin;

/**
 * 访问控制过滤器 (ACF) 是一种简单的授权方法，最适合只需要一些简单访问控制的应用程序。
 * 顾名思义，ACF 是一个动作过滤器，可以作为行为附加到控制器或模块上。
 * ACF 将检查一组访问规则以确保当前用户可以访问请求的操作。
 *
 * 要使用 AccessControl，请在应用程序配置中将其声明为行为。
 * 例如
 * ```
 * 'as access' => [
 *     'class' => 'admin\components\AccessControl',
 *     'allowActions' => ['site/login', 'site/error']
 * ]
 * ```
 *
 * @property User $user
 * @since 1.0
 */
class AccessControl extends \yii\base\ActionFilter
{
    /**
     * @var User User for check access.
     */
    private $_user = 'user';
    /**
     * @var array List of action that not need to check access.
     */
    public $allowActions = [];

    /**
     * Get user
     * @return Admin
     * @throws \yii\base\InvalidConfigException
     */
    public function getUser()
    {
        if (!$this->_user instanceof Admin) {
            $this->_user = Instance::ensure($this->_user, Admin::class);
        }
        return $this->_user;
    }

    /**
     * Set user
     * @param User|string $user
     */
    public function setUser($user)
    {
        $this->_user = $user;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $actionId = $action->getUniqueId();
        $user = $this->getUser();
        if (Helper::checkRoute('/' . $actionId, Yii::$app->getRequest()->get(), $user)) {
            return true;
        }
        $this->denyAccess($user);
    }

    /**
     * 拒绝用户访问。
     * 如果用户是访客，默认实现会将用户重定向到登录页面；
     * 如果用户已经登录，将抛出 403 HTTP 异常。
     * @param  Admin $user the current user
     * @throws ForbiddenHttpException if the user is already logged in.
     */
    protected function denyAccess($user)
    {
        if ($user->getIsGuest()) {
            $user->loginRequired();
        } else {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function isActive($action)
    {
        $uniqueId = $action->getUniqueId();
        if ($uniqueId === Yii::$app->getErrorHandler()->errorAction) {
            return false;
        }
        $user = $this->getUser();
        if($user->getIsGuest())
        {
            $loginUrl = null;
            if(is_array($user->loginUrl) && isset($user->loginUrl[0])){
                $loginUrl = $user->loginUrl[0];
                }else if(is_string($user->loginUrl)){
                    $loginUrl = $user->loginUrl;
                }
                if(!is_null($loginUrl) && trim($loginUrl,'/') === $uniqueId)
                {
                    return false;
                }
        }

        if ($this->owner instanceof Module) {
            $mid = $this->owner->getUniqueId();
            $id = $uniqueId;
            if ($mid !== '' && strpos($id, $mid . '/') === 0) {
                $id = substr($id, strlen($mid) + 1);
            }
        } else {
            $id = $action->id;
        }

        foreach ($this->allowActions as $route) {
            if (substr($route, -1) === '*') {
                $route = rtrim($route, "*");
                if ($route === '' || strpos($id, $route) === 0) {
                    return false;
                }
            } else {
                if ($id === $route) {
                    return false;
                }
            }
        }
        if ($action->controller->hasMethod('allowAction') && in_array($action->id, $action->controller->allowAction())) {
            return false;
        }
        return true;
    }
}
