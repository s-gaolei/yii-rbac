<?php

use yii\db\Migration;
use Gaolei\YiiRBAC\components\Configs;

class m160312_050000_create_adminer extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $userTable = Configs::instance()->userTable;
        $db = Configs::userDb();
        // Check if the table exists
        if ($db->schema->getTableSchema($userTable, true) === null) {
            $this->createTable($userTable, [
                'id' => $this->primaryKey(),
                'username' => $this->string(32)->notNull()->unique(),
                'realname' => $this->string(20)->notNull()->unique(),
                'phone' => $this->string()->notNull()->unique(),
                'level' => $this->tinyInteger()->notNull(),
                'state' => $this->tinyInteger()->notNull()->defaultValue(1),
                'auth_key' => $this->string(32)->notNull(),
                'password_hash' => $this->string()->notNull(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);
        }
    }

    public function down()
    {
        $userTable = Configs::instance()->userTable;
        $db = Configs::userDb();
        if ($db->schema->getTableSchema($userTable, true) !== null) {
            $this->dropTable($userTable);
        }
    }
}
